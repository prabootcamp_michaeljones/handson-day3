public class Binatang implements MahlukHidup, Jenis {
    private String nama;
    private int umur;
    private String bernafas;
    private String jenis;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getBernafas() {
        return bernafas;
    }

    public void setBernafas(String bernafas) {
        this.bernafas = bernafas;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

}
