public class Manusia implements MahlukHidup, Domisili {
    private String nama;
    private int umur;
    private String bernafas;
    private String domisili;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getBernafas() {
        return bernafas;
    }

    public void setBernafas(String bernafas) {
        this.bernafas = bernafas;
    }

    public String getDomisili() {
        return domisili;
    }

    public void setDomisili(String domisili) {
        this.domisili = domisili;
    }
}
