public interface MahlukHidup {
    int getUmur();

    void setUmur(int umur);

    String getBernafas();

    void setBernafas(String bernafas);

    String getNama();

    void setNama(String nama);
}
