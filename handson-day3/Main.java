public class Main {
    public static void main(String[] args) {
        Manusia ningen = new Manusia();
        ningen.setNama("Michael Jones");
        ningen.setBernafas("Paru Paru");
        ningen.setUmur(23);
        ningen.setDomisili("Batam, Kepulauan Riau");

        Binatang neko = new Binatang();
        neko.setNama("Kucing");
        neko.setBernafas("Paru - Paru");
        neko.setUmur(1);
        neko.setJenis("Persia");

        System.out.println("\nM A N U S I A");
        System.out.println("Nama: " + ningen.getNama());
        System.out.println("Domisili: " + ningen.getDomisili());
        System.out.println("Umur: " + ningen.getUmur() + " Tahun");
        System.out.println("Bernafas: " + ningen.getBernafas());

        System.out.println("\nB I N A T A N G");
        System.out.println("Nama Binatang: " + neko.getNama());
        System.out.println("Jenis: " + neko.getJenis());
        System.out.println("Umur: " + neko.getUmur() + " Tahun");
        System.out.println("Bernafas: " + neko.getBernafas());
    }
}
